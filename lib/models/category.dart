import 'package:budget_planner/models/db_table.dart';

class Category  extends DbTable{
  late int id;
  late String name;
  late bool expense;
  late int userID;

  static const TABLE_NAME = 'categories';

  Category();
  Category.fromMap(Map<String, dynamic> rowMap) : super.fromMap(rowMap){
    id = rowMap['id'];
    name = rowMap['name'];
    expense = rowMap['expense'] == 1;
    userID = rowMap['user_id'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['expense'] = expense ? 1 : 0;
    map['user_id'] = userID;
    return map;
  }
}