import 'package:budget_planner/utils/app_style_colors.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'app_text_widget.dart';

class LimitWidget extends StatelessWidget {

  final String msg;
  final void Function()? onTap;

  LimitWidget({ required this.msg, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(SizeConfig.scaleHeight(5)),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(25),
        boxShadow: <BoxShadow>[
          BoxShadow(
            offset: Offset(0, SizeConfig.scaleHeight(10)),
            color: AppStyleColors.SHADOW_COLOR,
            blurRadius: SizeConfig.scaleHeight(18),
            spreadRadius: 0,
          ),
        ],
      ),
        child: Row(
          children: [
            Icon(Icons.notifications_active,color: Colors.red.shade900,),
            SizedBox(width: 5,),
            AppTextWidget(
              msg,
              color: Colors.black,
              fontSize: SizeConfig.scaleTextFont(15),
              fontWeight: FontWeight.w500,
            ),
            Spacer(),
            IconButton(icon: Icon(Icons.clear), onPressed: onTap??(){})
          ],
        ),
    );
  }
}
